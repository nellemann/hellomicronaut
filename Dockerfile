###
### Stage 1 - Build Container
###

FROM eclipse-temurin:17-jdk as builder

# Copy our application sourcecode into the container
WORKDIR /tmp/app/
COPY . /tmp/app/

# Build our application from the sourcecode
RUN ./gradlew clean build --no-daemon



###
### Stage 2 - Runtime Container
###

FROM eclipse-temurin:17-jre-ubi9-minimal as runtime

# Copy our binary artifact from the previous building stage
COPY --from=builder /tmp/app/build/libs/hello-*-all.jar /opt/app/hello.jar

# Instructions for running our application
USER nobody
EXPOSE 8080/tcp
WORKDIR /opt/app
CMD ["java", "-jar", "/opt/app/hello.jar"]
