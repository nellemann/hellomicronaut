package hello;

import io.micronaut.http.MediaType;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.Deque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.views.View;
import jakarta.annotation.PostConstruct;

@Controller("/")
public class HelloController {

    private static final Logger log = LoggerFactory.getLogger(HelloController.class);

    private long counter = 0;
    private Deque<String> msgDeque = new ArrayDeque<String>();
    private String hostname;
    private String helloMessage;


    @PostConstruct
    void initialize() {
        log.info("initialize()");
        try {
            InetAddress inetadd = InetAddress.getLocalHost();
            hostname = inetadd.getHostName();
        } catch (UnknownHostException e) {
            log.error("", e);
            hostname = System.getenv("HOSTNAME");
        }

        String osName = System.getProperty("os.name");
        String osVersion = System.getProperty("os.version");
        String osArch = System.getProperty("os.arch");
        String javaName = System.getProperty("java.vm.name");
        String javaVersion = System.getProperty("java.vm.version");
        helloMessage = String.format("%s v%s, running %s %s on %s.", javaName, javaVersion, osName, osVersion, osArch);        
    }


    @Get("/")
    @View("index")
    public HttpResponse<?> index() {
        return HttpResponse.ok(CollectionUtils.mapOf("hostname", hostname, "info", helloMessage));
    }
   

    @Get("/ping")
    @View("pong")
    public HttpResponse<?> ping() {        
        return HttpResponse.ok(CollectionUtils.mapOf("hostname", hostname, "counter", counter, "messages", msgDeque));
    }


    @Post("/ping")
    @View("pong")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public HttpResponse<?> ping(String message) {
        log.info(message);
        if(!message.isEmpty()) {
            counter++;
            msgDeque.push(message);
        }
        if(msgDeque.size() > 25) {
            log.debug("Message deque: {}", msgDeque.size());
            msgDeque.removeLast();
        }
        return HttpResponse.ok(CollectionUtils.mapOf("hostname", hostname, "counter", counter, "messages", msgDeque));
    }

    
}
