# Hello Micronaut

Test "hello world" web application (written in the micronaut framework) that displays some information about the architecture it runs on, and allow users to enter some data, that will not persist or be shared across multiple instances.

![screenshot](doc/screenshot.png)

## Container

### Build container

```shell
docker build -t hellomicronaut .
```

### Run container 

```shell
docker run --name hello1 -p 8080:8080 hellomicronaut
```

## Development

Requires Java JDK version 11+.

### Build 

```shell
./gradlew build
```

### Run 

```shell
./gradlew run -t
```


## Test with browser

Connect to the container or localhost on the specified port:

<http://localhost:8080/>
